package com.peeps.mobapp.ui.main;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.peeps.mobapp.Downloadfile;
import com.peeps.mobapp.R;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.database.StoredFilesDatabase;
import com.peeps.mobapp.ui.category.CategoryActivity;
import com.peeps.mobapp.ui.main.adapter.StoredFilesAdapter;
import com.peeps.mobapp.ui.main.model.StoredFileModel;
import com.peeps.mobapp.ui.main.presenter.MainPresentorCompl;
import com.peeps.mobapp.ui.main.view.IMainView;
import com.peeps.mobapp.ui.main.view.MainActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class HomeFragment extends Fragment implements IMainView {
    View view;
    MainActivity activity;
    FloatingActionButton fabAddList;
    public static final int requestPermissionCode = 1002;
    Dialog dialogUrlType;
    RecyclerView rvStoredFiles;
    MainPresentorCompl mainPresentorCompl;

    @BindView(R.id.tvNoFileFound)
    TextView tvNoFileFound;
    private AdView mAdView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main, container, false);

        ButterKnife.bind(this, view);
        init();
        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        fabAddList = view.findViewById(R.id.fabAddList);
        fabAddList.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              showInsertLinkDialog();
                                          }
                                      }
        );


        MobileAds.initialize(activity, getString(R.string.add_mob_app_id));
        AdRequest adRequestInterstitial = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("621665CD8B002814D7F10A11481B273F")
                .build();


        mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        return view;
    }

    private void setStoredFiles() {
        mainPresentorCompl.getStoredFiles();
    }

    private void showInsertLinkDialog() {
        dialogUrlType = new Dialog(activity);

        dialogUrlType.requestWindowFeature(Window.FEATURE_NO_TITLE);
        try {
            Objects.requireNonNull(dialogUrlType.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        dialogUrlType.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogUrlType.setCancelable(true);
        dialogUrlType.setContentView(R.layout.dialog_add_playlist);

        TextView tvCancelUrl = dialogUrlType.findViewById(R.id.tvCancelUrl);
        TextView tvM3Url = dialogUrlType.findViewById(R.id.tvM3Url);
        final TextView btnCancelM3U = dialogUrlType.findViewById(R.id.btnCancelM3U);
        final TextView btnDoneM3U = dialogUrlType.findViewById(R.id.btnDoneM3U);
        final EditText etPlaylistLink = dialogUrlType.findViewById(R.id.etPlaylistLink);
        final EditText etPlaylistNameM3U = dialogUrlType.findViewById(R.id.etPlaylistNameM3U);
        final LinearLayout llSelectUrlType = dialogUrlType.findViewById(R.id.llSelectUrlType);
        final LinearLayout llEnterUrl = dialogUrlType.findViewById(R.id.llEnterUrl);

        tvCancelUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUrlType.dismiss();
            }
        });
        tvM3Url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, CategoryActivity.class));
                dialogUrlType.dismiss();
            }
        });
        tvM3Url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llSelectUrlType.setVisibility(View.GONE);
                llEnterUrl.setVisibility(View.VISIBLE);
            }
        });
        btnCancelM3U.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogUrlType.dismiss();
            }
        });

        btnDoneM3U.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(etPlaylistNameM3U.getText().toString().length() > 0)) {
                    activity.showSnackbar(etPlaylistLink, "Enter Name");
                } else if (!(etPlaylistLink.getText().toString().length() > 0))
                    activity.showSnackbar(etPlaylistLink, "Enter Link");
                else {
                    String fileName = etPlaylistNameM3U.getText().toString() + ".m3u";
                    String fileLink = etPlaylistLink.getText().toString();

                    downloadFile(new StoredFileModel(fileName, "0", fileLink));
                }
            }
        });
        dialogUrlType.show();

    }


    private void downloadFile(StoredFileModel storedFileModel) {
        if (!checkingPermissionIsEnabled()) {
            requestMultiplePermission();
        } else {
            downloadFileNow(storedFileModel);
        }

    }

    private void downloadFileNow(StoredFileModel storedFileModel) {
        File dir = new File(Constants.APP_FOLDER_PATH);
        try {
            if (!dir.exists())
                dir.mkdir();

        } catch (Exception e) {
            e.printStackTrace();
        }

        StoredFilesDatabase storedFilesHandler = new StoredFilesDatabase(activity);
        boolean fileExistInDatabase = false;
        for (int i = 0; i < storedFilesHandler.getStoredFiles().size(); i++) {
            if (storedFilesHandler.getStoredFiles().get(i).getName().equals(storedFileModel.getName())) {
                fileExistInDatabase = true;
            }
        }
        File file = new File(Constants.APP_FOLDER_PATH + storedFileModel.getName());
        if (file.exists() && fileExistInDatabase) {
            Toast.makeText(activity, getString(R.string.track_already_exists), Toast.LENGTH_SHORT).show();
        } else {
            dialogUrlType.dismiss();
            new Downloadfile(getActivity(), storedFileModel, file, HomeFragment.this).execute();
        }

    }

    private void init() {
        storedFileArray = new ArrayList<>();
        activity = (MainActivity) getActivity();
        rvStoredFiles = view.findViewById(R.id.rvStoredFiles);
        mainPresentorCompl = new MainPresentorCompl(this, activity);
    }

    private boolean checkingPermissionIsEnabled() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(activity, WRITE_EXTERNAL_STORAGE);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED;
    }


    private void requestMultiplePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]
                    {
                            READ_EXTERNAL_STORAGE,
                            WRITE_EXTERNAL_STORAGE

                    }, requestPermissionCode);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {


            case requestPermissionCode:

                boolean readStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean writeStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (readStorage && writeStorage) {

                }
                break;
        }
    }

    StoredFilesAdapter storedFilesAdapter;
    List<StoredFileModel> storedFileArray;

    public void onFileDownload(StoredFileModel storedFileModel) {

        StoredFilesDatabase storedFilesHandler = new StoredFilesDatabase(activity);
        storedFilesHandler.addFileToDatabase(storedFileModel);
        if (storedFileArray != null)
            storedFileArray.add(storedFileModel);
        if (storedFilesAdapter != null)
            storedFilesAdapter.notifyDataSetChanged();
        startActivity(new Intent(activity, CategoryActivity.class)
                .putExtra(Constants.PLAY_LIST_NAME, storedFileModel.getName()));

    }


    @Override
    public void onResume() {
        super.onResume();
        setStoredFiles();

    }

    @Override
    public void onGetStoredFileResult(boolean result, List<StoredFileModel> storedFileArrayList) {
        if (result) {
            setNoFileFoundVisible(false);
            this.storedFileArray = storedFileArrayList;
            Collections.reverse(storedFileArrayList);
            storedFilesAdapter = new StoredFilesAdapter(storedFileArray, activity, HomeFragment.this);
            rvStoredFiles.setLayoutManager(new LinearLayoutManager(activity));
            rvStoredFiles.setAdapter(storedFilesAdapter);
        } else {
            setNoFileFoundVisible(true);
        }
    }

    void setNoFileFoundVisible(boolean isVisible) {
        if (isVisible) {
            tvNoFileFound.setVisibility(View.VISIBLE);
            rvStoredFiles.setVisibility(View.GONE);
        } else {
            tvNoFileFound.setVisibility(View.GONE);
            rvStoredFiles.setVisibility(View.VISIBLE);
        }
    }

    public void showChangeFileNameDialog(final StoredFileModel storedFileModel, final int adapterPosition) {
        final Dialog changeNameDialog = new Dialog(activity);

        changeNameDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        try {
            Objects.requireNonNull(changeNameDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        changeNameDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        changeNameDialog.setCancelable(true);
        changeNameDialog.setContentView(R.layout.dialog_change_file_name);

        final EditText etPlaylistName = changeNameDialog.findViewById(R.id.etPlaylistName);
        final EditText etPlaylistLinkChange = changeNameDialog.findViewById(R.id.etPlaylistLinkChange);

        Button btnCancelRename = changeNameDialog.findViewById(R.id.btnCancelRename);
        Button btnSaveNewName = changeNameDialog.findViewById(R.id.btnSaveNewName);


        etPlaylistName.setText(storedFileModel.getName().replace(Constants.M3U_EXT, ""));
        etPlaylistLinkChange.setText(storedFileModel.getFileLink());

        if(etPlaylistName.length()>0)
        etPlaylistName.setSelection(etPlaylistName.getText().length());

        btnSaveNewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fileName = etPlaylistName.getText().toString() + Constants.M3U_EXT;
                if (fileName.length() > 0) {

                    StoredFilesDatabase storedFilesDatabase = new StoredFilesDatabase(activity);

                    boolean filnameExist = false;

                    for (int i = 0; i < storedFilesDatabase.getStoredFiles().size(); i++) {
                        if (storedFilesDatabase.getStoredFiles().get(i).getName().equals(fileName))
                            filnameExist = true;
                    }
                    if (filnameExist)
                        activity.showSnackbar(etPlaylistName, "File Already Exists. Please try a different name");
                    else {
                        File from = new File(Constants.APP_FOLDER_PATH + storedFileModel.getName());
                        File to = new File(Constants.APP_FOLDER_PATH, fileName);
                        if (from.renameTo(to)) {
                            String newLink = etPlaylistLinkChange.getText().toString();
                            storedFilesDatabase.updatePlaylistName(storedFileModel, fileName,newLink);
                            changeNameDialog.dismiss();

                            storedFileArray.set(adapterPosition, storedFileModel).setName(fileName);
                            storedFileArray.set(adapterPosition, storedFileModel).setFileLink(newLink);
                            storedFilesAdapter.notifyDataSetChanged();
                        } else
                            activity.showSnackbar(etPlaylistName, "Not able to rename. Please try again");

                    }
                }
            }
        });
        btnCancelRename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeNameDialog.dismiss();
            }
        });
        changeNameDialog.show();
    }
}
