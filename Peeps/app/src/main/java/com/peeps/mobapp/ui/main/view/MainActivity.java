package com.peeps.mobapp.ui.main.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;

import com.google.android.gms.ads.MobileAds;
import com.peeps.mobapp.R;
import com.peeps.mobapp.config.BaseActivity;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.ui.favorite.FavoriteFragment;
import com.peeps.mobapp.ui.main.HomeFragment;
import com.peeps.mobapp.ui.setting.SettingFragment;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCurrentTheme();
        setContentView(R.layout.activity_main2);
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        displayDefaultFragment();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!getSelectedTheme().equals(getPreviousTheme())) {
            setPreviousTheme(getSelectedTheme());
            finish();
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }
    }

    private void setAppTheme() {
            String selectedTheme = getSelectedTheme();
            switch (selectedTheme) {
                case Constants.BLACK_THEME:
                    setTheme(R.style.MyMaterialThemeBlueDark);
                    break;
                case Constants.DARK_GRAY_THEME:
                    setTheme(R.style.MyMaterialThemeDarkGray);
                    break;
                case Constants.YELLOW_THEME:
                    setTheme(R.style.MyMaterialThemeYellow);
                    break;
                case Constants.BLUE_DARK_THEME:
                    setTheme(R.style.MyMaterialThemeBlueDark);
                    break;
                case Constants.PURPLE_THEME:
                    setTheme(R.style.MyMaterialThemePurple);
                    break;
                case Constants.GRAY_THEME:
                    setTheme(R.style.MyMaterialThemeGray);
                    break;
                default:
                    break;
            }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    Fragment fragment;

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        int id = item.getItemId();
        fragment = null;

        if (id == R.id.nav_home) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbar.setTitle(getString(R.string.app_name));

                    fragment = new HomeFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();

                }
            }, 300);

            // Handle the camera action
        } else if (id == R.id.nav_favorite) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbar.setTitle(getString(R.string.favorite));

                    fragment = new FavoriteFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();

                }
            }, 300);


        } else if (id == R.id.nav_setting) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbar.setTitle(getString(R.string.settings));

                    fragment = new SettingFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();

                }
            }, 300);

        }
        if (fragment != null) {


        }

        return true;
    }

    private void displayDefaultFragment() {
        Fragment fragment = new HomeFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }

    }
}
