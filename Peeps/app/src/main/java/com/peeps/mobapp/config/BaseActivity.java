package com.peeps.mobapp.config;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.peeps.mobapp.R;

import java.io.File;


public class BaseActivity extends AppCompatActivity {

    AppCompatActivity activity;
    public Toolbar toolbar;
    private ProgressDialog pDialog;
    public TextView tvToolbarTitle;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = BaseActivity.this;
        pDialog = new ProgressDialog(BaseActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        context = this;



    }
    public void showToast(String message)
    {
        Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
    }
    public void showProgressDialog() {
        if (!pDialog.equals(null))
            if (!pDialog.isShowing())
                pDialog.show();
    }

    public void hideProgressDialog() {
        if (!pDialog.equals(null))
            if (pDialog.isShowing())
                pDialog.dismiss();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

//    ********* show toolbar **********
    public void initToolbar(String Name) {
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.gray));
        tvToolbarTitle =  toolbar.findViewById(R.id.tvToolbarTitle);
        tvToolbarTitle.setText(Name);
    }

    public void showSnackbar(EditText editText, String message) {
        hideKeyBoard(editText);

        Snackbar snackbar = Snackbar
                .make(editText, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public void hideKeyBoard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public boolean fileExists()
    {
        File dir = new File(Constants.APP_FOLDER_PATH);
        try {
            if (!dir.exists()) {
                dir.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    return dir.exists();
    }
    public  void setAddCount(int count) {
        SharedPreferences.Editor editor = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt("add_show_count", count );
        editor.apply();

    }

    public  int getAddCount() {
        SharedPreferences prefs = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE);
        return prefs.getInt("add_show_count", 0);
    }
    public  void setSelectedTheme(String theme) {
        SharedPreferences.Editor editor = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("app_theme", theme );
        editor.apply();
    }

    public  String getSelectedTheme() {
        SharedPreferences prefs = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("app_theme", Constants.APP_FOLDER_PATH);
    }
    public  void setPreviousTheme(String theme) {
        SharedPreferences.Editor editor = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("previous_app_theme", theme );
        editor.apply();
    }

    public  String getPreviousTheme() {
        SharedPreferences prefs = getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("previous_app_theme", Constants.APP_FOLDER_PATH);
    }
    public void setCurrentTheme() {
        String selectedTheme = getSelectedTheme();
        switch (selectedTheme) {
            case Constants.BLACK_THEME:
                getApplicationContext().setTheme(R.style.MyMaterialThemeBlueDark);
                break;
            case Constants.DARK_GRAY_THEME:
                getApplicationContext().setTheme(R.style.MyMaterialThemeDarkGray);
                break;
            case Constants.YELLOW_THEME:
                getApplicationContext().setTheme(R.style.MyMaterialThemeYellow);
                break;
            case Constants.BLUE_DARK_THEME:
                getApplicationContext().setTheme(R.style.MyMaterialThemeBlueDark);
                break;
            case Constants.PURPLE_THEME:
                setTheme(R.style.MyMaterialThemePurple);
                break;
            case Constants.GRAY_THEME:
                setTheme(R.style.MyMaterialThemeGray);
                break;
            default:
                break;
        }

    }

}