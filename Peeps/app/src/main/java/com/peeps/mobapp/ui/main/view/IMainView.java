package com.peeps.mobapp.ui.main.view;

import com.peeps.mobapp.ui.main.model.StoredFileModel;

import java.util.List;

public interface IMainView {
   void onGetStoredFileResult(boolean result, List<StoredFileModel> storedFileArray);
}
