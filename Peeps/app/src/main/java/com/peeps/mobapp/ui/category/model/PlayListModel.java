package com.peeps.mobapp.ui.category.model;

import java.io.Serializable;

public class PlayListModel implements Serializable {

    private String videoName;
    private String videoUrl;
    private String videoImage;
    private String groupName;
    private String category;

    public String getChannelId() {
        return channelId;
    }

    private String channelId;


    public String isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    private String isFavorite;


    public String getVideoName() {
        return videoName;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getVideoImage() {
        return videoImage;
    }


    public String getGroupName() {
        return groupName;
    }


    public String getCategory() {
        return category;
    }

    public PlayListModel()
    {}
    public PlayListModel(String videoName, String videoUrl, String videoImage, String category,String groupName,String channelId,String isFavorite) {
        this.videoName = videoName;
        this.videoUrl = videoUrl;
        this.videoImage = videoImage;
        this.category = category;
        this.groupName = groupName;
        this.channelId = channelId;
        this.isFavorite = isFavorite;
    }
}
