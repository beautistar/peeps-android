package com.peeps.mobapp.ui.main.model;

public class StoredFileModel {
    String name;
    String id;

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }

    String fileLink;

    public  StoredFileModel(String name, String id,String fileLink)
    {
        this.name = name;
        this.id = id;
        this.fileLink = fileLink;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
