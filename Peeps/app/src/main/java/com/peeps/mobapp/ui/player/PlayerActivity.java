package com.peeps.mobapp.ui.player;

import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.peeps.mobapp.R;
import com.peeps.mobapp.config.BaseActivity;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.videocontrollerview.VideoGestureListener;
import com.peeps.mobapp.videocontrollerview.ViewGestureListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayerActivity extends BaseActivity implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, VideoGestureListener {
    String videoUrl;
    VideoView videoPlayerMain;
    private GestureDetector mGestureDetector;
    @BindView(R.id.ivLive)
    ImageView ivLive;
    @BindView(R.id.pbPlayer)
    ProgressBar pbPlayer;
    @BindView(R.id.progress_center)
    ProgressBar mCenterProgress;
    @BindView(R.id.image_center_bg)
    ImageView mCenterImage;
    @BindView(R.id.layout_center)
    FrameLayout mCenterLayout;
    View decorView;

    private InterstitialAd mInterstitialAd;
    int stopPosition;

    private float mCurBrightness = -1;
    private int mCurVolume = -1;

    private AudioManager mAudioManager;
    private int mMaxVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.MyMaterialTheme);

        setContentView(R.layout.activity_player);

        getIntents();
        ButterKnife.bind(this);

        decorView = getWindow().getDecorView();
        mCenterLayout.setVisibility(View.GONE);


        videoPlayerMain = findViewById(R.id.videoPlayerMain);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoPlayerMain);
        videoPlayerMain.setMediaController(mediaController);
        videoPlayerMain.setVideoPath(videoUrl);
        videoPlayerMain.requestFocus();
        videoPlayerMain.setOnErrorListener(this);
        videoPlayerMain.setOnPreparedListener(this);
        videoPlayerMain.start();
        videoPlayerMain.setKeepScreenOn(true);

        showSystemUI();


        if (getAddCount() == 1) {
            Log.e("ShowingCount", getAddCount() + " ShowingCount");
            showAdd();

        } else if (getAddCount() == 3)
            setAddCount(0);
        setAddCount(getAddCount() + 1);

        setGestureListener();
    }

    private void setGestureListener() {
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mGestureDetector = new GestureDetector(this, new ViewGestureListener(this, this));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoPlayerMain != null) {
            videoPlayerMain.seekTo(stopPosition);
            videoPlayerMain.start(); //Or use resume() if it doesn't work. I'm not sure
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (videoPlayerMain != null) {
            int stopPosition = videoPlayerMain.getCurrentPosition();
            videoPlayerMain.pause();
            outState.putInt("position", stopPosition);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                mCurVolume = -1;
                mCurBrightness = -1;
                mCenterLayout.setVisibility(View.GONE);
//                break;// do need bread,should let gestureDetector to handle event
            default://gestureDetector handle other MotionEvent
                if (mGestureDetector != null)
                    mGestureDetector.onTouchEvent(event);
        }
        return true;

    }

    private void updateVolume(float percent) {

        mCenterLayout.setVisibility(View.VISIBLE);

        if (mCurVolume == -1) {
            mCurVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (mCurVolume < 0) {
                mCurVolume = 0;
            }
        }

        int volume = (int) (percent * mMaxVolume) + mCurVolume;
        if (volume > mMaxVolume) {
            volume = mMaxVolume;
        }

        if (volume < 0) {
            volume = 0;
        }
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

        int progress = volume * 100 / mMaxVolume;
        mCenterProgress.setProgress(progress);
    }

    /**
     * update brightness by seek percent
     *
     * @param percent seek percent
     */
    private void updateBrightness(float percent) {

        if (mCurBrightness == -1) {
            mCurBrightness = getWindow().getAttributes().screenBrightness;
            if (mCurBrightness <= 0.01f) {
                mCurBrightness = 0.01f;
            }
        }

        mCenterLayout.setVisibility(View.VISIBLE);

        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = mCurBrightness + percent;
        if (attributes.screenBrightness >= 1.0f) {
            attributes.screenBrightness = 1.0f;
        } else if (attributes.screenBrightness <= 0.01f) {
            attributes.screenBrightness = 0.01f;
        }
        getWindow().setAttributes(attributes);

        float p = attributes.screenBrightness * 100;
        mCenterProgress.setProgress((int) p);

    }


    private void showAdd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.play_screen_add));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.e("TAG", "The interstitial wasn't loaded yet.");
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdClosed() {
            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) //To fullscreen
            hideSystemUI();
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            showSystemUI();
    }

    private void getIntents() {
        videoUrl = getIntent().getStringExtra(Constants.VIDEO_LINK);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        double videoDuration = videoPlayerMain.getDuration();
        if (videoDuration == -1)
            ivLive.setVisibility(View.VISIBLE);
        else
            ivLive.setVisibility(View.GONE);

        pbPlayer.setVisibility(View.GONE);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        pbPlayer.setVisibility(View.GONE);
        return false;
    }

    private void hideSystemUI() {
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void onSingleTap() {

    }

    @Override
    public void onHorizontalScroll(boolean seekForward) {

    }

    private boolean mCanControlVolume;
    private boolean mCanControlBrightness;

    @Override
    public void onVerticalScroll(float percent, int direction) {

        Log.e("percent", percent + "  ----  " + direction);
        if (direction == ViewGestureListener.SWIPE_LEFT) {


//            if (direction == ViewGestureListener.SWIPE_LEFT) {
//                if(mCanControlBrightness) {
            mCenterImage.setImageResource(R.drawable.video_bright_bg);
            updateBrightness(percent);
//                }
        } else {
//                if(mCanControlVolume) {
            mCenterImage.setImageResource(R.drawable.video_volume_bg);
            updateVolume(percent);
//                }
//            }

        }
    }
}
