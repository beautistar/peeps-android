package com.peeps.mobapp.ui.setting;

import android.os.Bundle;

import com.peeps.mobapp.R;
import com.peeps.mobapp.config.BaseActivity;
import com.peeps.mobapp.config.Constants;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThemesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCurrentTheme();

        setContentView(R.layout.activity_themes);
        ButterKnife.bind(this);
        initToolbar("Themes");
    }



    @OnClick(R.id.btnBlackTheme)
    void onBlackThemeClicked() {
        setSelectedTheme(Constants.GRAY_THEME);
        restartActivity();
    }

    @OnClick(R.id.btnThemeDarkGray)
    void onDarkGrayTheme() {
        setSelectedTheme(Constants.DARK_GRAY_THEME);
        restartActivity();
    }


    @OnClick(R.id.btnThemeYellow)
    void onYellowThemeClicked() {
        setSelectedTheme(Constants.YELLOW_THEME);
        restartActivity();
    }

    @OnClick(R.id.btnBlueTheme)
    void onBlueThemeClicked() {
        setSelectedTheme(Constants.BLUE_DARK_THEME);
        restartActivity();
    }

    @OnClick(R.id.btnThemePurple)
    void onPurpleThemeClicked() {
        setSelectedTheme(Constants.PURPLE_THEME);
        restartActivity();
    }

    @OnClick(R.id.btnGrayTheme)
    void onGrayThemeClicked() {
        setSelectedTheme(Constants.GRAY_THEME);
        restartActivity();
    }

    private void restartActivity() {
        finish();
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

}
