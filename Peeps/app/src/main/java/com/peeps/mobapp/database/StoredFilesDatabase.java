package com.peeps.mobapp.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.ui.main.model.StoredFileModel;

import java.io.File;
import java.util.ArrayList;

public class StoredFilesDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_STORED_FILES = "stored_files";

    private Context context;

    // practice_tracks Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FILE_NAME = "file_name";
    private static final String KEY_FILE_LINK = "file_link";

    // Database Name
    private static final String DATABASE_NAME = "storedFilesManager";

    public StoredFilesDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRACTICE_TABLE = "CREATE TABLE " + TABLE_STORED_FILES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_FILE_NAME + " TEXT,"
                + KEY_FILE_LINK + " TEXT" + ")";
        db.execSQL(CREATE_PRACTICE_TABLE);
    }

    public void addFileToDatabase(StoredFileModel storedFileModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FILE_NAME, storedFileModel.getName());
        values.put(KEY_FILE_LINK, storedFileModel.getFileLink());
        db.insert(TABLE_STORED_FILES, null, values);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORED_FILES);
        // Create tables again
        onCreate(db);
    }

    public ArrayList<StoredFileModel> getStoredFiles() {
        ArrayList<StoredFileModel> practiceTracksArray = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_STORED_FILES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                StoredFileModel storedFileModel=
                        new StoredFileModel(cursor.getString(1),
                                cursor.getString(0) + "",
                                cursor.getString(2) + ""
                        );
                File dir = new File(Constants.APP_FOLDER_PATH);

                File file = new File(Constants.APP_FOLDER_PATH + storedFileModel.getName());

                if ( dir.exists() && file.exists())
                    practiceTracksArray.add(storedFileModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return practiceTracksArray;
    }

    public void deleteSingleTrack(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_STORED_FILES, KEY_ID + " = "+id,null);

    }
    public void updatePlaylistName(StoredFileModel storedFileModel ,String name,String newLink) {
        SQLiteDatabase db = this.getWritableDatabase();

        String strFilter = KEY_ID+"=" + storedFileModel.getId();
        ContentValues args = new ContentValues();
        args.put(KEY_FILE_NAME, name);
        args.put(KEY_FILE_LINK, newLink);
        db.update(TABLE_STORED_FILES, args, strFilter, null);
        db.close();


    }
}
