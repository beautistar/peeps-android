package com.peeps.mobapp.ui.favorite;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.peeps.mobapp.R;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.database.FavoriteDatabase;
import com.peeps.mobapp.ui.category.model.PlayListModel;
import com.peeps.mobapp.ui.player.adapter.PlaylistAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteFragment extends Fragment {
    View view;
    RecyclerView rvFavorite;

    @BindView(R.id.tvNoFavoriteTracksFound)
    TextView tvNoFavoriteTracksFound;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favorite, container, false);

        ButterKnife.bind(this, view);
        initViews();
        FavoriteDatabase favoriteDatabase = new FavoriteDatabase(getActivity());
        List<PlayListModel> favoriteListAdapter = favoriteDatabase.getFavoriteChannels();
        if (favoriteListAdapter.size() > 0) {
            setFavoriteListVisible(true);
            for (int i = 0; i < favoriteListAdapter.size(); i++) {
                Log.e("name", favoriteDatabase.getFavoriteChannels().get(0).getVideoName());
                PlaylistAdapter playlistAdapter = new PlaylistAdapter(favoriteDatabase.getFavoriteChannels(), getActivity(), Constants.FAVORITE);
                rvFavorite.setLayoutManager(new LinearLayoutManager(getActivity()));
                rvFavorite.setAdapter(playlistAdapter);
            }
        } else {
            setFavoriteListVisible(false);
        }
        return view;
    }

    private void initViews() {
        rvFavorite = view.findViewById(R.id.rvFavorite);
    }

    private void setFavoriteListVisible(boolean isVisible) {
        if (isVisible) {
            rvFavorite.setVisibility(View.VISIBLE);
            tvNoFavoriteTracksFound.setVisibility(View.GONE);
        } else {
            rvFavorite.setVisibility(View.VISIBLE);
            tvNoFavoriteTracksFound.setVisibility(View.GONE);
        }
    }
}
