package com.peeps.mobapp.config;

public interface DownloadCompletionListener {
    void downloadTrackListener(boolean result, String name,String link ,String message, boolean showMessage);

}
