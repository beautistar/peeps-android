package com.peeps.mobapp.ui.category;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.peeps.mobapp.R;
import com.peeps.mobapp.config.BaseActivity;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.config.RecyclerItemClickListener;
import com.peeps.mobapp.player.M3UParser;
import com.peeps.mobapp.player.M3UPlaylist;
import com.peeps.mobapp.ui.category.adapter.CategoryAdapter;
import com.peeps.mobapp.ui.category.model.CategoryModel;
import com.peeps.mobapp.ui.category.model.PlayListModel;
import com.peeps.mobapp.ui.playlist.PlayListActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryActivity extends BaseActivity {
    M3UParser parser;
    RecyclerView rvPlayLIst;
    List<PlayListModel> playListArray;
    String playListName;
    List<CategoryModel> categoryArray;
    @BindView(R.id.pbPlayList)
    ProgressBar pbPlayList;
    CategoryAdapter categoryAdapter;
    List<CategoryModel> searchArray;

    SearchView searchView;
    MenuItem searchItem;
    boolean isSearchEnbaled;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentTheme();
        setContentView(R.layout.activity_category);

        ButterKnife.bind(this);
        getIntents();
        initToolbar("Peeps");
        init();
        getPlayList();


        rvPlayLIst.addOnItemTouchListener(

                new RecyclerItemClickListener(CategoryActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        List<PlayListModel> tempChannels = new ArrayList<>();
                        if(!isSearchEnbaled) {
                            for (int i = 0; i < playListArray.size(); i++) {
                                if (categoryArray.get(position).getCategoryName().equals(playListArray.get(i).getCategory())) {
                                    PlayListModel playListModel = playListArray.get(i);
                                    tempChannels.add(playListModel);
                                }

                            }
                        }
                        else
                        {
                            for (int i = 0; i < playListArray.size(); i++) {
                                if (searchArray.get(position).getCategoryName().equals(playListArray.get(i).getCategory())) {
                                    PlayListModel playListModel = playListArray.get(i);
                                    tempChannels.add(playListModel);
                                }

                            }

                        }
                        startActivity(new Intent(CategoryActivity.this, PlayListActivity.class)
                                .putExtra("playListArray", (Serializable) tempChannels)

                        );
                    }
                })
        );

    }

    private void getIntents() {
        playListName = getIntent().getStringExtra(Constants.PLAY_LIST_NAME);
    }

    private void init() {
        isSearchEnbaled = false;
        searchArray = new ArrayList<>();
        rvPlayLIst = findViewById(R.id.rvPlayLIst);
    }

    private void getPlayList() {
        File dir = new File(Constants.APP_FOLDER_PATH);
        if (dir.exists())
            Log.e("fileExists", "fileExists");
        File filepath = new File(Constants.APP_FOLDER_PATH + playListName);
        if (filepath.exists())

            parser = new M3UParser();

        new _loadFile().execute(filepath.getPath());

    }

    class _loadFile extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                FileInputStream is = new FileInputStream(new File(strings[0])); // if u r trying to open file from asstes InputStream is = getassets.open(); InputStream
                M3UPlaylist playlist = parser.parseFile(is);

                playListArray = new ArrayList<>();
                for (int i = 0; i < playlist.getPlaylistItems().size(); i++) {
                    String channelName = playlist.getPlaylistItems().get(i).getItemName();
                    String iconUrl = playlist.getPlaylistItems().get(i).getItemIcon();
                    String channalIcon = iconUrl.substring(0, iconUrl.indexOf("group-title"));
                    String groupTitle = iconUrl.substring(iconUrl.indexOf("group-title")).replace("group-title", "");

//                    FavoriteDatabase favoriteDatabase = new FavoriteDatabase(CategoryActivity.this);

//                    String favChannelUrl, favChannelName;
//                    String isFavorite = "0";
//                    for (int j = 0; j < favoriteDatabase.getFavoriteChannels().size(); j++) {
//                        favChannelName = favoriteDatabase.getFavoriteChannels().get(j).getVideoName();
//                        favChannelUrl = favoriteDatabase.getFavoriteChannels().get(j).getVideoUrl();
//
//                        if (favChannelName.equals(channelName) && favChannelUrl.equals(iconUrl))
//                            isFavorite = "1";
//                    }
                    playListArray.add(new PlayListModel(playlist.getPlaylistItems().get(i).getItemName(),
                            playlist.getPlaylistItems().get(i).getItemUrl(),
                            channalIcon, groupTitle, groupTitle, "0","0"));
                }
                return true;
            } catch (Exception e) {
                Log.e("error", "_loadFile: " + e.toString());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            pbPlayList.setVisibility(View.GONE);
            super.onPostExecute(aBoolean);
            categoryArray = new ArrayList<>();
            Collections.sort(playListArray, new Comparator<PlayListModel>() {
                @Override
                public int compare(PlayListModel playListModel, PlayListModel t1) {
                    return playListModel.getCategory().compareToIgnoreCase(t1.getCategory());
                }
            });
            int groupcount = 0;
            String currentGroupName;
            String previouName = "";
            int previousCount = 0;
            for (int i = 0; i < playListArray.size(); i++) {
                currentGroupName = playListArray.get(i).getGroupName();
                if (i > 1) {
                    if (previouName.equals(currentGroupName)) {
                        ++groupcount;
                    } else {
                        categoryArray.add(new CategoryModel(previouName, groupcount + 1));
                        groupcount = 0;
                    }
                }
                previouName = playListArray.get(i).getGroupName();
            }


            categoryAdapter = new CategoryAdapter(categoryArray, CategoryActivity.this);
            rvPlayLIst.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
            rvPlayLIst.setAdapter(categoryAdapter);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.playlist_search, menu);

        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) CategoryActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchItem.setVisible(false);
        }
        if (searchView != null) {
            searchView.setSearchableInfo(Objects.requireNonNull(searchManager).getSearchableInfo(this.getComponentName()));
        }
        final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                isSearchEnbaled = true;
                searchArray.clear();
                if (playListArray.size() > 0) {
                    for (int i = 0; i < categoryArray.size(); i++) {

                        if (categoryArray.get(i).getCategoryName() != null)
                            if (categoryArray.get(i).getCategoryName().length() > 0)
                                if (categoryArray.get(i).getCategoryName().toLowerCase().contains(newText.toLowerCase())) {
                                    Log.e("searchPos", categoryArray.get(i).getCategoryName());
                                    CategoryModel playListModel = categoryArray.get(i);
                                    searchArray.add(playListModel);
                                }
                    }
                    categoryAdapter = new CategoryAdapter(searchArray, CategoryActivity.this);
                    rvPlayLIst.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
                    rvPlayLIst.setAdapter(categoryAdapter);
                }
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("onQueryTextSubmit", "onQueryTextSubmit");
                searchArray.clear();
                return true;
            }
        };
        searchView.clearFocus();
        searchItem.collapseActionView();
        searchItem.setVisible(true);
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

}
