package com.peeps.mobapp.ui.category.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.peeps.mobapp.R;
import com.peeps.mobapp.ui.category.model.CategoryModel;

import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context context;
    private List<CategoryModel> playListArray;

    public CategoryAdapter(List<CategoryModel> programeModel, Context context) {
        this.playListArray = programeModel;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_category, parent, false);

        return new CategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryAdapter.MyViewHolder holder, int position) {
        final CategoryModel playListModel = playListArray.get(position);

        holder.tvChannelGroupCount.setText(playListModel.getGroupCount()+"");
        holder.tvChannelGroup.setText(playListModel.getCategoryName());


    }


    @Override
    public int getItemCount() {
        return playListArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvChannelGroup, tvChannelGroupCount;

        public MyViewHolder(View view) {
            super(view);
            tvChannelGroup = view.findViewById(R.id.tvChannelGroup);
            tvChannelGroupCount = view.findViewById(R.id.tvChannelGroupCount);

        }
    }
}

