package com.peeps.mobapp.ui.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.peeps.mobapp.R;
import com.peeps.mobapp.ui.setting.languge.LanguageActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingFragment extends Fragment {
    View view;
    @BindView(R.id.rlLanguage)
    RelativeLayout rlLanguage;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_setting, container, false);

        ButterKnife.bind(this, view);


        return view;
    }
    @OnClick(R.id.rlLanguage)
    public void onLanguageClicked()
    {
        startActivity(new Intent(getActivity(), LanguageActivity.class));
    }
    @OnClick(R.id.rlThemes)
    public void onChangeThemeClicked()
    {
        startActivity(new Intent(getActivity(), ThemesActivity.class));
    }
}
