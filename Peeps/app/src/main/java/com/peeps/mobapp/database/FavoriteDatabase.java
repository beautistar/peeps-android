package com.peeps.mobapp.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.peeps.mobapp.ui.category.model.PlayListModel;

import java.util.ArrayList;
import java.util.List;

public class FavoriteDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_FAVORITE_FILES = "favorite_cannel";
    public static final String DATABASE_NAME_FAV = "favorite_database";

    private Context context;

    // practice_tracks Table Columns names
    private static final String KEY_CHANNEL_ID = "id";
    private static final String KEY_CHANNEL_NAME_FAV = "channel_name";
    private static final String KEY_CHANNEL_LINK_FAV = "channel_link";
    private static final String KEY_CHANNEL_IMAGE_FAV = "channel_image";
    private static final String KEY_GROUP_NAME_FAV = "group_name";
    private static final String KEY_CATEGORY_FAV = "category_name";
    private static final String KEY_IS_FAV_FAV = "is_favorite";


    public FavoriteDatabase(Context context) {
        super(context, DATABASE_NAME_FAV, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRACTICE_TABLE = "CREATE TABLE " + TABLE_FAVORITE_FILES + "("
                + KEY_CHANNEL_ID + " INTEGER PRIMARY KEY,"
                + KEY_CHANNEL_NAME_FAV + " TEXT,"
                + KEY_CHANNEL_LINK_FAV + " TEXT,"
                + KEY_CHANNEL_IMAGE_FAV + " TEXT,"
                + KEY_GROUP_NAME_FAV + " TEXT,"
                + KEY_CATEGORY_FAV + " TEXT,"
                +  KEY_IS_FAV_FAV+ " TEXT" + ")";
        db.execSQL(CREATE_PRACTICE_TABLE);
    }

    public void addFileToDatabase(PlayListModel playListModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CHANNEL_NAME_FAV, playListModel.getVideoName());
        values.put(KEY_CHANNEL_LINK_FAV, playListModel.getVideoUrl());
        values.put(KEY_CHANNEL_IMAGE_FAV, playListModel.getVideoImage());
        values.put(KEY_GROUP_NAME_FAV, playListModel.getGroupName());
        values.put(KEY_CATEGORY_FAV, playListModel.getCategory());
        values.put(KEY_IS_FAV_FAV, playListModel.isFavorite());

        db.insert(TABLE_FAVORITE_FILES, null, values);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITE_FILES);
        onCreate(db);
    }

    public List<PlayListModel> getFavoriteChannels() {
        ArrayList<PlayListModel> practiceTracksArray = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_FAVORITE_FILES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                PlayListModel playListModel=
                        new PlayListModel(cursor.getString(1)+"",
                                cursor.getString(2) + "",
                                cursor.getString(3) + "",
                                cursor.getString(5) + "",
                                cursor.getString(4) + "",
                                cursor.getString(5)+"",
                                cursor.getString(6)+""
                        );

                    Log.e("playListModel",playListModel.getVideoName());

                    practiceTracksArray.add(playListModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return practiceTracksArray;
    }

    public String getSingleTrack(PlayListModel playListModel) {
        String selectQuery = "SELECT * FROM " + TABLE_FAVORITE_FILES
                + " WHERE " + KEY_CHANNEL_NAME_FAV + " =? AND " + KEY_CHANNEL_LINK_FAV + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{playListModel.getVideoName(), playListModel.getVideoUrl()});
        if (cursor.moveToFirst()) {

            if(cursor.getString(6).equals("1"))
                cursor.close();
                return "1";
            } while (cursor.moveToNext());
//        }
        cursor.close();
        return "0";
    }


    public void deleteChannel(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FAVORITE_FILES, KEY_CHANNEL_ID + " = "+id,null);
        db.close();
    }

    public String deleteFavoritetrack(PlayListModel playListModel) {
//        String selectQuery = "SELECT * FROM " + TABLE_FAVORITE_FILES
//                + " WHERE " + KEY_CHANNEL_NAME_FAV + " =? AND " + KEY_CHANNEL_LINK_FAV + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FAVORITE_FILES, KEY_CHANNEL_NAME_FAV + " =? AND "+KEY_CHANNEL_LINK_FAV+ " =?",new String[]{playListModel.getVideoName(), playListModel.getVideoUrl()});
        db.close();
//        Cursor cursor = db.rawQuery(selectQuery, new String[]{playListModel.getVideoName(), playListModel.getVideoUrl()});
//        if (cursor.moveToFirst()) {
//
//            if(cursor.getString(6).equals("1"))
//                cursor.close();
//            return "1";
//        } while (cursor.moveToNext());
////        }
//        cursor.close();
        return "0";
    }


    public void updatePlaylistName(PlayListModel playListModel,String isFavorite) {
        SQLiteDatabase db = this.getWritableDatabase();

        String strFilter = KEY_CHANNEL_NAME_FAV+"=? AND "  + KEY_CHANNEL_LINK_FAV + " =?";
        ContentValues args = new ContentValues();
        args.put(KEY_CHANNEL_LINK_FAV, isFavorite);
        db.update(TABLE_FAVORITE_FILES, args, strFilter, new String[]{playListModel.getVideoName(), playListModel.getVideoUrl()});
        db.close();

    }
}
