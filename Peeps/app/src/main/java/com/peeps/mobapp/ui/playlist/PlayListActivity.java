package com.peeps.mobapp.ui.playlist;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.peeps.mobapp.R;
import com.peeps.mobapp.config.BaseActivity;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.ui.category.model.PlayListModel;
import com.peeps.mobapp.ui.player.adapter.PlaylistAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PlayListActivity extends BaseActivity {
    List<PlayListModel> playListArray;
    List<PlayListModel> searchArray;
    RecyclerView rvPlayList;
    PlaylistAdapter playlistAdapter;
    SearchView searchView;
    MenuItem searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentTheme();
        setContentView(R.layout.activity_play_list);
        playListArray = new ArrayList<>();
        playListArray = (List<PlayListModel>) getIntent().getSerializableExtra("playListArray");
        Log.e("questions", playListArray.get(0).getCategory());
        searchArray = new ArrayList<>();
//        searchArray = playListArray;
        initToolbar("Play List");
        initViews();
        setPlayListAdapter();
    }

    private void setPlayListAdapter() {
        playlistAdapter = new PlaylistAdapter(playListArray, PlayListActivity.this, Constants.PLAYLIST);
        rvPlayList.setLayoutManager(new LinearLayoutManager(PlayListActivity.this));
        rvPlayList.setAdapter(playlistAdapter);

    }

    private void initViews() {
        rvPlayList = findViewById(R.id.rvPlayList);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.playlist_search, menu);

        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) PlayListActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchItem.setVisible(false);
        }
        if (searchView != null) {
            searchView.setSearchableInfo(Objects.requireNonNull(searchManager).getSearchableInfo(this.getComponentName()));
        }
        final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                searchArray.clear();
                    if(playListArray.size()>0) {
                        for (int i = 0; i < playListArray.size(); i++) {

                            if (playListArray.get(i).getVideoName() != null)
                                if (playListArray.get(i).getVideoName().length() > 0)
                                    if (playListArray.get(i).getVideoName().toLowerCase().contains(newText.toLowerCase())) {
                                        Log.e("searchPos", playListArray.get(i).getVideoName());
                                        PlayListModel playListModel = playListArray.get(i);
                                        searchArray.add(playListModel);
                                    }
                        }

                        playlistAdapter = new PlaylistAdapter(searchArray, PlayListActivity.this,Constants.PLAYLIST);
                        rvPlayList.setLayoutManager(new LinearLayoutManager(PlayListActivity.this));
                        rvPlayList.setAdapter(playlistAdapter);
                    }
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
            Log.e("onQueryTextSubmit","onQueryTextSubmit");
                searchArray.clear();
                return true;
            }
        };
        searchView.clearFocus();
        searchItem.collapseActionView();
        searchItem.setVisible(true);
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
