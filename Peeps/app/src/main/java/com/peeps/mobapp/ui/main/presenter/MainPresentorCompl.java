package com.peeps.mobapp.ui.main.presenter;

import android.content.Context;

import com.peeps.mobapp.database.StoredFilesDatabase;
import com.peeps.mobapp.ui.main.model.StoredFileModel;
import com.peeps.mobapp.ui.main.view.IMainView;

import java.util.ArrayList;
import java.util.List;

public class MainPresentorCompl implements IMainPresentor {
    private IMainView iMainView;
    private Context context;
    private List<StoredFileModel> storedArray;
    public MainPresentorCompl(IMainView iMainView,Context context)
    {
        this.iMainView = iMainView;
        this.context = context;
    }
    @Override
    public void getStoredFiles() {
        storedArray = new ArrayList<>();

        StoredFilesDatabase storedFilesHandler = new StoredFilesDatabase(context);
        storedArray = storedFilesHandler.getStoredFiles();
        if(storedArray.size()>0)
            iMainView.onGetStoredFileResult(true,storedArray);
        else
            iMainView.onGetStoredFileResult(false,storedArray);
    }
}
