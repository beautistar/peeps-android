package com.peeps.mobapp.config;

import android.os.Environment;

public class Constants {
    public final static String PLAY_LIST_NAME = "play_list_name";
    public final static String PLAY_LIST_LINK = "play_list_link";
    public final static String VIDEO_LINK = "video_link";
    public static final String MY_PREFS_NAME = "peeps_pref";
    public static final String M3U_EXT = ".m3u";

    public static final String DATABASE_NAME = "storedFilesManager";
    public static final String APP_FOLDER_PATH = Environment.getExternalStorageDirectory().toString()+ "/.peeps/";
    public static final String FAVORITE = "favorite";
    public static final String PLAYLIST = "playlist";
    public static final String BLACK_THEME = "black_theme";
    public static final String BLUE_DARK_THEME = "blue_dark_theme";
    public static final String YELLOW_THEME = "yellow_theme";
    public static final String DARK_GRAY_THEME = "dark_gray_theme";
    public static final String GRAY_THEME = "gray_theme";
    public static final String PURPLE_THEME = "purple_theme";
}
