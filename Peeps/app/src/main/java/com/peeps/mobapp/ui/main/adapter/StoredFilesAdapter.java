package com.peeps.mobapp.ui.main.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peeps.mobapp.R;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.database.StoredFilesDatabase;
import com.peeps.mobapp.ui.category.CategoryActivity;
import com.peeps.mobapp.ui.main.HomeFragment;
import com.peeps.mobapp.ui.main.model.StoredFileModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StoredFilesAdapter extends RecyclerView.Adapter<StoredFilesAdapter.MyViewHolder> {
    private Context context;
   private List<StoredFileModel> storedFileArray;
    HomeFragment homeFragment;
    public StoredFilesAdapter(List<StoredFileModel> storedFileArray, Context context,HomeFragment homeFragment) {
        this.storedFileArray = storedFileArray;
        this.context = context;
        this.homeFragment = homeFragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_stored_files, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder,  int position) {

        holder.tvFileName.setText(storedFileArray.get(position).getName().replace(Constants.M3U_EXT,""));

        holder.tvFileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CategoryActivity.class)
                        .putExtra(Constants.PLAY_LIST_NAME, storedFileArray.get(holder.getAdapterPosition()).getName()));
            }
        });

        holder.llDeleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAlertDialog(storedFileArray.get(holder.getAdapterPosition()).getId(),holder.getAdapterPosition());
            }
        });

        holder.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoredFileModel storedFileModel = storedFileArray.get(holder.getAdapterPosition());
                homeFragment.showChangeFileNameDialog(storedFileModel,holder.getAdapterPosition() );
            }
        });
    }

    private void showAlertDialog(final String id, final int position)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_delete)
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete this file")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        StoredFilesDatabase storedFilesHandler = new StoredFilesDatabase(context);
                        storedFilesHandler.deleteSingleTrack(id);
                        storedFileArray.remove(position);
                        notifyDataSetChanged();

                    }
                })
                //set negative button
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();



    }


    @Override
    public int getItemCount() {
        return storedFileArray.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFileName)
        TextView tvFileName;
        @BindView(R.id.llDeleteFile)
        LinearLayout llDeleteFile;
        @BindView(R.id.llEdit)
        LinearLayout llEdit;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }
    }


}
