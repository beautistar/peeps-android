package com.peeps.mobapp.ui.player.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.peeps.mobapp.R;
import com.peeps.mobapp.config.Constants;
import com.peeps.mobapp.database.FavoriteDatabase;
import com.peeps.mobapp.ui.category.model.PlayListModel;
import com.peeps.mobapp.ui.player.PlayerActivity;

import java.util.List;


public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.MyViewHolder> {

    Context context;
    private List<PlayListModel> playListArray;
    String adapterCalledFrom;

    public PlaylistAdapter(List<PlayListModel> programeModel, Context context, String adapterCalledFrom) {
        this.playListArray = programeModel;
        this.context = context;
        this.adapterCalledFrom = adapterCalledFrom;
    }

    @NonNull
    @Override
    public PlaylistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_playlist, parent, false);

        return new PlaylistAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PlaylistAdapter.MyViewHolder holder, int position) {
        final PlayListModel playListModel = playListArray.get(position);
        holder.tvChannelName.setText(playListModel.getVideoName());
        holder.tvChannelURl.setText(playListModel.getVideoUrl());

        Glide.with(context)
                .load(playListModel.getVideoImage().trim())
                .into(holder.ivChannelImage);

        FavoriteDatabase favoriteDatabase = new FavoriteDatabase(context);


        String favChannelName = playListModel.getVideoName(), favChannelUrl = playListModel.getVideoUrl();

        if (favChannelName != null && favChannelUrl != null)
        {
            if (playListModel.getVideoName().length() > 0 && playListModel.getVideoUrl().length() > 0) {
                String isFav = favoriteDatabase.getSingleTrack(playListModel);

                Log.e("favChannelName",favChannelName+"    "+favChannelUrl);
                if (isFav.equals("1")) {
                    holder.ivFavorite.setImageResource(R.drawable.ic_favorite_star);
                    playListArray.set(position, playListModel).setIsFavorite("1");
                } else {
                    playListArray.set(position, playListModel).setIsFavorite("0");
                    holder.ivFavorite.setImageResource(R.drawable.ic_favorite_star_empty);
                }
                }
                else
                holder.ivFavorite.setImageResource(R.drawable.ic_favorite_star_empty);

        }
            else
            holder.ivFavorite.setImageResource(R.drawable.ic_favorite_star_empty);
        {

        }
        holder.llPlaylistLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playListModel.getVideoName() != null && playListModel.getVideoUrl() != null)
                    if (playListModel.getVideoName().length() > 0 && playListModel.getVideoUrl().length() > 0)
                        context.startActivity(new Intent(context, PlayerActivity.class)
                                .putExtra(Constants.VIDEO_LINK, playListArray.get(holder.getAdapterPosition()).getVideoUrl()));
            }
        });
        holder.llFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String favChannelName = playListModel.getVideoName(), favChannelUrl = playListModel.getVideoUrl();
                FavoriteDatabase favoriteDatabase = new FavoriteDatabase(context);

                if (favChannelName != null && favChannelUrl != null) {
                    if (playListModel.getVideoName().length() > 0 && playListModel.getVideoUrl().length() > 0) {
                    Log.e("isFavorite", playListModel.isFavorite());
                    if (playListModel.isFavorite().equals("0")) {
                        favoriteDatabase.addFileToDatabase(playListModel);

                        playListArray.set(holder.getAdapterPosition(), playListModel).setIsFavorite("1");
                        holder.ivFavorite.setImageResource(R.drawable.ic_favorite_star);
                        notifyDataSetChanged();
                    } else {
                        playListArray.set(holder.getAdapterPosition(), playListModel).setIsFavorite("0");
                        holder.ivFavorite.setImageResource(R.drawable.ic_favorite_star_empty);
                        favoriteDatabase.deleteFavoritetrack(playListModel);
                        if (adapterCalledFrom.equals(Constants.FAVORITE)) {
                            playListArray.remove(holder.getAdapterPosition());
                            notifyDataSetChanged();
                        }
                    }
                }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return playListArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvChannelName, tvChannelURl;
        ImageView ivChannelImage, ivFavorite;
        LinearLayout llPlaylistLayout, llFavorite;

        public MyViewHolder(View view) {
            super(view);
            ivChannelImage = view.findViewById(R.id.ivChannelImage);
            ivFavorite = view.findViewById(R.id.ivFavorite);
            tvChannelName = view.findViewById(R.id.tvChannelName);
            tvChannelURl = view.findViewById(R.id.tvChannelURl);
            llPlaylistLayout = view.findViewById(R.id.llPlaylistLayout);
            llFavorite = view.findViewById(R.id.llFavorite);

        }
    }
}

