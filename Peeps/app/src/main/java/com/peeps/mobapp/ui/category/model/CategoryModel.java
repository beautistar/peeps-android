package com.peeps.mobapp.ui.category.model;

public class CategoryModel {
    String categoryName;
    int groupCount;

    public CategoryModel(String categoryName, int groupCount) {
        this.categoryName = categoryName;
        this.groupCount = groupCount;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(int groupCount) {
        this.groupCount = groupCount;
    }
}
